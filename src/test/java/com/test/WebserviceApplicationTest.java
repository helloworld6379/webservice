package com.test;

import com.webservice.service.TFamilyService;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.apache.cxf.service.model.*;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Base64;
import java.util.Collection;
import java.util.List;

@SpringBootTest
public class WebserviceApplicationTest {

    @Test
    public  void contextLoads() throws Exception {
        String param = "xml内容";
        System.out.println(param);
        method2(param);

    }

    public static void method1(String param) {
        try {
            // 接口地址
            String address = "http://localhost:8080/mytest/TFamilyService?wsdl";
            // 代理工厂
            JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
            // 设置代理地址
            jaxWsProxyFactoryBean.setAddress(address);
            // 设置接口类型
            jaxWsProxyFactoryBean.setServiceClass(TFamilyService.class);
            // 创建一个代理接口实现
            TFamilyService tf = (TFamilyService) jaxWsProxyFactoryBean.create();
            // 数据准备
            // String userName = "Leftso";
            // 调用代理接口的方法调用并返回结果
            String result = tf.chairman(param);
            System.out.println("返回结果:\n" + result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 动态调用方式
     *
     * @throws Exception
     */
    public static void method2(String param) throws Exception {
        // 创建动态客户端
        JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
        Client client = dcf
                .createClient("http://localhost:8080/mytest/TFamilyService?wsdl");
        // getUser 为接口中定义的方法名称 张三为传递的参数 返回一个Object数组
        Object[] objects = client.invoke("chairman", param);
        for (Object object : objects) {
            System.err.println(object);
        }
    }


    /**
     * 动态调用方式
     *
     * @throws Exception
     */
    public static void cl3() throws Exception {
        JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
        Client client = dcf
                .createClient("http://www.webxml.com.cn/WebServices/IpAddressSearchWebService.asmx?wsdl");

        List<ServiceInfo> infos = client.getEndpoint().getService()
                .getServiceInfos();
        for (ServiceInfo serviceInfo : infos) {
            Collection<BindingInfo> dd = serviceInfo.getBindings();
            for (BindingInfo bindingInfo2 : dd) {
                Collection<BindingOperationInfo> bb = bindingInfo2
                        .getOperations();
                for (BindingOperationInfo bindingOperationInfo : bb) {
                    System.out.println(bindingOperationInfo.getName().getLocalPart());
                    BindingMessageInfo inputMessageInfo = bindingOperationInfo
                            .getInput();
                    inputMessageInfo.getMessageInfo();
                    List<MessagePartInfo> parts = inputMessageInfo
                            .getMessageParts();
                    for (MessagePartInfo messagePartInfo : parts) {
                        System.out.println(messagePartInfo.getName().getLocalPart());
                    }
                }
            }
        }
        // for (BindingOperationInfo operationInfo :
        // bindingInfo.getOperations()) {
        // System.out.println(operationInfo.getInput());
        // System.out.println(operationInfo.getOutput());
        // System.out.println(operationInfo.getName());
        // System.out.println(operationInfo.getName().getLocalPart());
        // }

    }

}
