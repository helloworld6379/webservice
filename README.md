# SpringBootWebservice

## 1. 引入除springboot外pom文件
Spring Boot 已经集成了 cxf 所需要其他的相关依赖
```xml
<!--cxf-->
<dependency>
    <groupId>org.apache.cxf</groupId>
    <artifactId>cxf-spring-boot-starter-jaxws</artifactId>
    <version>3.2.5</version>
</dependency>
```
##2. 接口

```java
@WebService(name = "TFamilyService",  // 对外暴露的服务名称
        targetNamespace = "http://service.webservice.com")  // 命名空间,一般是接口的包名倒序
public interface TFamilyService {
    @WebMethod
    public String chairman(@WebParam(name = "reqXml") String reqXml);
}
```
##3. 实现类
```java
@WebService(name = "TFamilyService",// 对外暴露的服务名称
        targetNamespace = "http://service.webservice.com",  // 命名空间,一般是接口的包名倒序
        endpointInterface = "com.webservice.service.TFamilyService") // 接口地址
@Component
public class TFamilyServiceImpl implements TFamilyService{
    @Override
    public String chairman(String reqXml) {
        return reqXml + ": 执行成功";
    }
}
```
##4. 配置类，发布服务
##5. 测试








