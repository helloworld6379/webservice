package com.webservice.config;

import com.webservice.service.TFamilyService;
import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.ws.Endpoint;

@Configuration
public class CXFConfig {

    @Autowired
    private TFamilyService tFamilyService;

     @Bean(name = Bus.DEFAULT_BUS_ID) //也可以采用自动注入的方式
     public SpringBus springBus() {
     return new SpringBus();
     }

     @Bean // 修改默认的servlet名称为mytest 默认为services  这个bean不注入就为services
     public ServletRegistrationBean servletRegistration() {
     return new ServletRegistrationBean(new CXFServlet(), "/mytest/*");
     }

    // @Bean  //也可以采用自动注入的方式 如上
    // public TFamilyService tFamilyService() {
    // return new TFamilyServiceImpl();
    // }

    @Bean
    public Endpoint endpoint(){
        EndpointImpl endpoint = new EndpointImpl(springBus(),tFamilyService);
        endpoint.publish("/TFamilyService");
        return endpoint;
    }


}
