package com.webservice.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(name = "TFamilyService",  // 对外暴露的服务名称
            targetNamespace = "http://service.webservice.com")  // 命名空间,一般是接口的包名倒序
public interface TFamilyService {
    @WebMethod
    public String chairman(@WebParam(name = "reqXml") String reqXml);
}
