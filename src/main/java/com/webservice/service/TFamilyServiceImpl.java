package com.webservice.service;

import org.springframework.stereotype.Component;

import javax.jws.WebService;

@WebService(name = "TFamilyService",// 对外暴露的服务名称
        targetNamespace = "http://service.webservice.com",  // 命名空间,一般是接口的包名倒序
        endpointInterface = "com.webservice.service.TFamilyService") // 接口地址
@Component
public class TFamilyServiceImpl implements TFamilyService{
    @Override
    public String chairman(String reqXml) {
        return reqXml + ": 执行成功";
    }
}
